﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CALCULADORA
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        string operacion;
        double signo, num1, num2, resultado;

        private void btnTres_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text + "3";
        }

        private void btnCuatro_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text + "4";
        }

        private void btnCinco_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text + "5";
        }

        private void btnSeis_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text + "6";
        }

        private void btnSiete_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text + "7";
        }

        private void btnOcho_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text + "8";
        }

        private void btnNueve_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text + "9";
        }

        private void txtResultado_TextChanged(object sender, EventArgs e)
        {
          
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            txtResultado.Clear();
        }

        private void btnDividir_Click(object sender, EventArgs e)
        {
            num1 = txtResultado.Text;
            txtResultado.Text = "/";
            operacion = "/";

        }

        private void btnMenos_Click(object sender, EventArgs e)
        {
            num1 = txtResultado.Text;
            txtResultado.Text = "";
            operacion = "-";
        }

        private void btnDos_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text + "2";
        }

        private void btnMultiplicar_Click(object sender, EventArgs e)
        {
            num1 = txtResultado.Text;
            txtResultado.Text = "*";
            operacion = "*";
        }

        private void btnMas_Click(object sender, EventArgs e)
        {
            num1 = txtResultado.Text;
            txtResultado.Text = "+";
            operacion = "+";
        }

        private void btnUno_Click(object sender, EventArgs e)
        {        
            txtResultado.Text = txtResultado.Text + "1";
        }

        private void btnCero_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text + "0";
        }

        private void btnSigno_Click(object sender, EventArgs e)
        {
            signo = double.Parse(txtResultado.Text); // 15
            signo = signo - (signo * 2);             // signo = -15 - (15 * 2) -> -15 - (-30)
            txtResultado.Text = signo.ToString();    // signo = 15
        }

        private void btnIgual_Click(object sender, EventArgs e)
        {
            if  (num1 = 0)
                MessageBox.Show("No se realizo la operacion");
            else
                num2 = txtResultado.Text;
            if (operacion == "+")
               txtResultado.Text = (num1 + num2);
            else
            if (operacion == "-")
               txtResultado.Text = (num1 - num2);
            else
            if (operacion == "*")
               txtResultado.Text = (num1 * num2);
            if (operacion == "/")
               txtResultado.Text = (num1 / num2);
        }
    }
} 
